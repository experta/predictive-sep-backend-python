# Predictive SEP Back-End

_Aplicación Back-End desarrollado para el Sistema Experto Predictivo de Dynamo, desarrollado en Django/Python3 que alimenta la aplicación Front-End "Predictive-SEP-Frontend"._

* Predictive-SEP-Frontend: https://bitbucket.org/experta/predictive-sep-frontend/src/master/